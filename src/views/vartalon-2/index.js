import React from 'react';
import { Col } from 'reactstrap';
import $ from 'jquery';
import { Link } from 'react-router-dom'

class Vartalon2 extends React.Component {
    componentDidMount() {
        this.changePage();
    }

    changePage() {
        $(document).ready(function () {
            $(document).on('touchmove', function () {
                window.location = '/vartalon-3';
            })
        })
    }

    render() {
        return (
            <Col lg="12" md="12" xs="12" sm="12">
                <img className="img" src="images/imagen2.png" alt="vartalon" />
                <Link to="/Vartalon-3">
                    <div className="link"></div>
                </Link>

                <Link to="/Vartalon-4">
                    <div className="link1"></div>
                </Link>

                <Link to="/Vartalon-5">
                    <div className="link2"></div>
                </Link>

                <Link to="/Vartalon-6">
                    <div className="link3"></div>
                </Link>

                <Link to="/Vartalon-7">
                    <div className="link4"></div>
                </Link>

                <Link to="/Vartalon-8">
                    <div className="link0"></div>
                </Link>
            </Col>
        );
    }
}

export default Vartalon2;