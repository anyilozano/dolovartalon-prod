import React from 'react';
import { Col, Row } from 'reactstrap';
import $ from 'jquery';
import { Link } from "react-router-dom";

class Vartalon3 extends React.Component {
    componentDidMount() {
        this.changePage();
    }

    changePage(){
        $(document).ready(function(){
            $(document).on('touchmove', function(){
                window.location = '/vartalon-4';
            })
        })
    }

    render(){
        return(
            <Col className="vartalon3" lg="12" md="12" xs="12" sm="12">
                <Row>
                    <Col  lg="6" md="6" xs="6" sm="6">
                        <img src="images/cuadro-3.png" className="grafica animate__animated animate__slideInLeft" alt="vartalon" />
                    </Col>

                    <Col  lg="6" md="6" xs="6" sm="6">
                        <img src="images/cuadro-5.png" className=" grafica1 animate__animated animate__fadeInDownBig" alt="vartalon" />
                    </Col>

                    <Col  lg="6" md="6" xs="6" sm="6">
                        <img src="images/Recurso 2@2x-8.png" className=" grafica2 animate__animated animate__fadeInLeft" alt="vartalon" />
                    </Col>

                </Row>

                <Link to="/Vartalon-2">
                    <div className="link01" ></div>
                </Link>
            </Col>
        );  
    }
}

export default Vartalon3; 