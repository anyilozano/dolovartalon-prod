import React from 'react';
import { Col } from 'reactstrap';
import { Link } from 'react-router-dom'
import $ from 'jquery';

class Vartalon3 extends React.Component {
    componentDidMount() {
        $('body').removeAttr('id');
        this.changePage();
    }

    changePage(){
        $(document).ready(function(){
            $(document).on('touchmove', function(){
                window.location = '/';
            })
        })
    }

    render(){
        return(
            <Col className="vartalon8" lg="12" md="12" xs="12" sm="12">
                <img className="plazo animate__animated animate__slideInUp" src="images/vaso.png"/> 

                <img className="plazo1 animate__animated animate__slideInLeft" src="images/rojo.png"/>
                <img className="plazo2 animate__animated animate__slideInLeft" src="images/blue.png"/>

                <img className="plazo3 animate__animated animate__zoomIn" src="images/Recurso 10@2x-8.png"/>
                <img className="plazo4 animate__animated animate__zoomIn" src="images/Recurso 11@2x-8.png"/>

                <img className="plazo5 animate__animated animate__zoomInRight" src="images/Recurso 12@2x-8.png"/>


                <Link to="/Vartalon-2">
                    <div className="link5"></div>
                </Link>
            </Col>
            
        );  
    }
}

export default Vartalon3; 