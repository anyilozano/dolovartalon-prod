import React from 'react';
import { Col, Row } from 'reactstrap';
import $ from 'jquery';
import { Link } from "react-router-dom";


class Vartalon3 extends React.Component {
    componentDidMount() {
        $('body').removeAttr('id');
        this.changePage();
    }

    changePage(){
        $(document).ready(function(){
            $(document).on('touchmove', function(){
                window.location = '/vartalon-8';
            })
        })
    }

    render(){
        return(
            <Col className="vartalon7" lg="12" md="12" xs="12" sm="12">
                <Row className="m-0">
                    <Col lg="6" md="6" xs="6" sm="6">
                        <img src="images/eficacia.png" className=" eficacia animate__animated animate__fadeInDownBig" alt="vartalon"/>
                    </Col>

                    <Col lg="6" md="6" xs="6" sm="6">
                        <img src="images/seguridad.png" className="eficacia1 animate__animated animate__bounceInUp" alt="vartalon"/>
                    </Col>
                </Row>

                <Link to="/Vartalon-2">
                    <div className="link02"></div>
                </Link>
            </Col>
            
        );  
    }
}

export default Vartalon3; 