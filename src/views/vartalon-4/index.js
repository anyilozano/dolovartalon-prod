import React from 'react';
import { Col } from 'reactstrap';
import $ from 'jquery';
import { Link } from "react-router-dom";


class Vartalon4 extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            bandera: '0'
        }

        this.diapositive = this.diapositive.bind(this);
    }
    componentDidMount() {
        $('body').attr('id', 'body');
        this.changePage();
        this.diapositive();
    }

    changePage() {
        $(document).ready(function () {
            $(document).on('touchmove', function () {
                window.location = '/vartalon-5';
            })
        })
    }

    diapositive() {
        let bandera = 0;
        $(document).ready(function () {
            $(document).on('click', function () {
                
        
                if (bandera === 0) {
                    $('.nfk').fadeIn('slow');
                    bandera++
                } else {
                    if (bandera === 1) {
                        $('.il').fadeToggle(1000);
                        bandera++
                    } else {
                        if (bandera === 2) {
                            $('.estres').fadeToggle(1000);
                            bandera++
                        } else {
                            if (bandera === 3) {
                                $('.on').fadeToggle(1000);
                                bandera++
                            } else {
                                if (bandera === 4) {
                                    $('.colagenasa').fadeToggle(1000);
                                    bandera++
                                } else {
                                    if (bandera === 5) {
                                        $('.cox').fadeToggle(1000);
                                        bandera++
                                    } else {
                                        if (bandera === 6) {
                                            $('.pge').fadeToggle(1000);
                                            bandera++
                                        } else {
                                            if (bandera === 7) {
                                                $('.colagenasa').addClass('animate__pulse');
                                                bandera++
                                            } else {
                                                if (bandera === 8) {
                                                    $('.acido').fadeToggle(1000);
                                                    bandera++
                                                } else {
                                                    if (bandera === 9) {
                                                        $('.equis').fadeToggle(1000);
                                                        bandera++
                                                    } else {
                                                        if (bandera === 10) {
                                                            $('.oste').fadeToggle(1000);
                                                            bandera++
                                                        } else {
                                                            if (bandera === 11) {
                                                                // $('body').addClass('body');
                                                                bandera++
                                                            } else {
                                                                if (bandera === 12) {
                                                                    $('.gluco').fadeToggle(1000);
                                                                    bandera++
                                                                } else {
                                                                    if (bandera === 13) {
                                                                        $('.flecha').fadeToggle(1000);
                                                                        $('.equis1').fadeToggle(1000);
                                                                        bandera++
                                                                    } else {
                                                                        if (bandera === 14) {
                                                                            $('.gluco2').fadeToggle(1000);
                                                                            bandera++
                                                                        } else {
                                                                            if (bandera === 15) {
                                                                                $('.flecha2').fadeToggle(1000);
                                                                                $('.equis2').fadeToggle(1000);
                                                                                bandera++
                                                                            } else {
                                                                                if (bandera === 16) {
                                                                                    $('.melo').fadeToggle(1000);
                                                                                    bandera++
                                                                                } else {
                                                                                    if (bandera === 17) {
                                                                                        $('.flecha3').fadeToggle(1000);
                                                                                        $('.equis3').fadeToggle(1000);
                                                                                        bandera++
                                                                                    } else {
                                                                                        if (bandera === 18) {
                                                                                            $('.equis4').fadeToggle(1000);
                                                                                            bandera++
                                                                                        } else {
                                                                                            if (bandera === 19) {
                                                                                                $('.gluco3').fadeToggle(1000);
                                                                                                bandera++
                                                                                            } else {
                                                                                                if (bandera === 20) {
                                                                                                    $('.melo1').css('visibility', 'visible');
                                                                                                    bandera++
                                                                                                } else {
                                                                                                    if (bandera === 21) {
                                                                                                        $('.derecha').fadeToggle(1000);
                                                                                                        bandera++
                                                                                                    } else {
                                                                                                        if (bandera === 22) {
                                                                                                            $('.equis').fadeOut(1000);
                                                                                                            bandera++
                                                                                                        } else {
                                                                                                            if (bandera === 23) {
                                                                                                                $('.cuadro').fadeToggle(1000);
                                                                                                                bandera++
                                                                                                            } else {
                                                                                                                if (bandera === 24) {
                                                                                                                    $('.mejoria').fadeToggle(1000);
                                                                                                                    bandera++
                                                                                                                } else {
                                                                                                                    if (bandera === 25) {
                                                                                                                        $('.mejoria').fadeOut(1000);
                                                                                                                        $('.mejoria1').fadeToggle(1000);
                                                                                                                        $('.equis').fadeOut(1000);
                                                                                                                        $('.equis1').fadeOut(1000);
                                                                                                                        $('.equis2').fadeOut(1000);
                                                                                                                        $('.equis3').fadeOut(1000);
                                                                                                                        $('.equis4').fadeOut(1000);
                                                                                                                        // $('.melo1').fadeOut(1000);
                                                                                                                        $('.cuadro').fadeOut(1000);
                                                                                                                        bandera++
                                                                                                                    } else {
                                                                                                                        if (bandera === 26) {
                                                                                                                            $('.melo1').css('visibility', 'hidden');
                                                                                                                            // $('.derecha').css('margin-top', '-1.8%');
                                                                                                                            bandera++
                                                                                                                        } else {
                                                                                                                            if (bandera === 27) {
                                                                                                                                $('.text').fadeToggle(1000);
                                                                                                                                $('.mejoria1').fadeOut(1000);
                                                                                                                                bandera++
                                                                                                                            } else {
                                                                                                                                if (bandera === 28) {
                                                                                                                                    $('.text').fadeOut(1000);
                                                                                                                                    $('.gluco').css('visibility', 'hidden');
                                                                                                                                    $('.flecha').css('visibility', 'hidden');
                                                                                                                                    $('.gluco2').css('visibility', 'hidden');
                                                                                                                                    $('.flecha2').css('visibility', 'hidden');
                                                                                                                                    $('.gluco3').css('visibility', 'hidden');
                                                                                                                                    $('.derecha').css('visibility', 'hidden');
                                                                                                                                    $('.melo').css('visibility', 'hidden');
                                                                                                                                    $('.flecha3').css('visibility', 'hidden');
        
                                                                                                                                    $('.text1').fadeToggle(1000);
                                                                                                                                    $('.blanco').fadeToggle(1000);
                                                                                                                                    bandera++
                                                                                                                                }
                                                                                                                                else {
                                                                                                                                    if (bandera === 29) {
                                                                                                                                    $('.blanco2').fadeToggle(1000);
                                                                                                                                    $('.text2').fadeToggle(1000);
        
                                                                                                                                    $('.blanco').fadeOut(1000);
                                                                                                                                    $('.text1').fadeOut(1000);
                                                                                                                                        bandera++
                                                                                                                                    } else {
                                                                                                                                        if (bandera === 30) {
                                                                                                                                        $('.text3').fadeToggle(1000);
        
                                                                                                                                        $('.text2').fadeOut(1000);
                                                                                                                                            bandera++
                                                                                                                                        } else {
                                                                                                                                            if (bandera === 31) {
                                                                                                                                            $('.text4').fadeToggle(1000);
        
                                                                                                                                            $('.blanco2').fadeOut(1000);
                                                                                                                                            $('.text3').fadeOut(1000);
                                                                                                                                                bandera++
                                                                                                                                            } else {
                                                                                                                                                if (bandera === 32) {
                                                                                                                                                $('.text5').fadeToggle(1000);
                                                                                                                                                $('.gluco').css('visibility', 'visible');
                                                                                                                                                $('.flecha').css('visibility', 'visible');
                                                                                                                                                $('.gluco2').css('visibility', 'visible');
                                                                                                                                                $('.flecha2').css('visibility', 'visible');
        
        
                                                                                                                                                $('.text4').fadeOut(1000);
                                                                                                                                                    bandera++
                                                                                                                                                } else {
                                                                                                                                                    if (bandera === 33) {
                                                                                                                                                    $('.text6').fadeToggle(1000);
                                                                                                                                                    $('.gluco').css('visibility', 'visible');
                                                                                                                                                    $('.flecha').css('visibility', 'visible');
                                                                                                                                                    $('.gluco2').css('visibility', 'visible');
                                                                                                                                                    $('.flecha2').css('visibility', 'visible');
                                                                                                                                                    $('.gluco3').css('visibility', 'visible');
                                                                                                                                                    $('.derecha').css('visibility', 'visible');
        
        
                                                                                                                                                    $('.text5').fadeOut(1000);
                                                                                                                                                        bandera++
                                                                                                                                                    } else {
                                                                                                                                                        if (bandera === 34) {
                                                                                                                                                        $('.text7').fadeToggle(1000);
                                                                                                                                                        $('.gluco').css('visibility', 'visible');
                                                                                                                                                        $('.flecha').css('visibility', 'visible');
                                                                                                                                                        $('.gluco2').css('visibility', 'visible');
                                                                                                                                                        $('.flecha2').css('visibility', 'visible');
                                                                                                                                                        $('.gluco3').css('visibility', 'visible');
                                                                                                                                                        $('.derecha').css('visibility', 'visible');
                                                                                                                                                        $('.melo').css('visibility', 'visible');
                                                                                                                                                        $('.flecha3').css('visibility', 'visible');
        
                                                                                                                                                        $('.text6').fadeOut(1000);
                                                                                                                                                            bandera++
                                                                                                                                                        } else {
                                                                                                                                                            if (bandera === 35) {
                                                                                                                                                            $('.text8').fadeToggle(1000);
                                                                                                                                                            $('.gluco').css('visibility', 'visible');
                                                                                                                                                            $('.flecha').css('visibility', 'visible');
                                                                                                                                                            $('.gluco2').css('visibility', 'visible');
                                                                                                                                                            $('.flecha2').css('visibility', 'visible');
                                                                                                                                                            $('.gluco3').css('visibility', 'visible');
                                                                                                                                                            $('.derecha').css('visibility', 'visible');
                                                                                                                                                            $('.melo').css('visibility', 'visible');
                                                                                                                                                            $('.flecha3').css('visibility', 'visible');
                                                                                                                                                            $('.melo1').css('visibility', 'visible');
        
        
        
                                                                                                                                                            $('.text7').fadeOut(1000);
                                                                                                                                                                bandera++
                                                                                                                                                            } else {
                                                                                                                                                                if (bandera === 36) {
        
                                                                                                                                                                $('.text8').fadeOut(1000);
                                                                                                                                                                    bandera++
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
        
                }
            })
        })
        

    }



    render() {
        return (
            <Col className="vartalon4" lg="12" md="12" xs="12" sm="12">
                <div className="nfk">
                    <img className="tamaño" src="images/NFK.png" alt="vartalon"/>
                </div>
                <div className="il">
                    <img className="tamaño" src="images/IL 1 B.png" alt="vartalon"/>
                </div>
                <div className="estres">
                    <img className="tamaño" src="images/ESTRES OXIDATIVO.png" alt="vartalon"/>
                </div>
                <div className="on">
                    <img className="tamaño" src="images/ON.png"alt="vartalon" />
                </div>
                <div className="colagenasa animate__animated">
                    <img className="tamaño" src="images/METALOPROTEASAS.png" alt="vartalon"/>
                </div>
                <div className="cox">
                    <img className="tamaño" src="images/COS- 2.png" alt="vartalon" />
                </div>
                <div className="pge">
                    <img className="tamaño" src="images/PGE2.png" alt="vartalon"/>
                </div>
                <div className="acido">
                    <img className="tamaño" src="images/ACIDO HIALURONICO.png" alt="vartalon" />
                </div>
                <div className="oste">
                    <img className="tamaño" src="images/OSTEOARTRITIS.png" alt="vartalon" />
                </div>
                {/* <div className="fondo">
                    <img className="tamaño" src="images/Recurso 3@2x-8.png"/>
                </div>  */}
                <div className="equis">
                    <p>x</p>
                </div>
                <div className="gluco">
                    <img className="tamaño" src="images/GLUCOSAMINA.png" alt="vartalon"/>
                </div>
                <div className="flecha">
                    <img className="tamaño" src="images/FELCHA ROJO 2.png" alt="vartalon"/>
                </div>
                <div className="equis1">
                    <p>x</p>
                </div>
                <div className="gluco2">
                    <img className="tamaño" src="images/GLUCOSAMINA2.png" alt="vartalon"/>
                </div>
                <div className="equis2">
                    <p>x</p>
                </div>
                <div className="flecha2">
                    <img className="tamaño" src="images/FELCHA ROJO 1.png" alt="vartalon"/>
                </div>
                <div className="melo">
                    <img className="tamaño" src="images/MELOXICAN.png" alt="vartalon"/>
                </div>
                <div className="flecha3">
                    <img className="tamaño" src="images/FELCHA ROJO 2.png" alt="vartalon"/>
                </div>
                <div className="equis3">
                    <p>x</p>
                </div>
                <div className="equis4">
                    <p>x</p>
                </div>
                <div className="gluco3">
                    <img className="tamaño" src="images/GLUCOSAMINA2.png" alt="vartalon"/>
                </div>
                <div className="melo1">
                    <img className="tamaño" src="images/MELOXICAN.png" alt="vartalon"/>
                </div>
                <div className="derecha">
                    <img className="tamaño" src="images/FLECHA VERDE.png" alt="vartalon"/>
                </div>
                <div className="dolo">
                    <img className="tamaño" src="images/varatalon dolo.png" alt="vartalon"/>
                </div>
                <div className="cuadro"></div>

                {/* pop ups */}
                <div className="mejoria">
                    <p>Mejoria</p>
                </div>
                <div className="mejoria1">
                    <p>Objectivo:
                        <br /> Demostrar la  <br /> sinergia del <br /> mecanismo de <br /> acción de <br /> meloxicam y <br /> glucosamina en el <br /> tratamiento de la <br /> osteoartritis
                    </p>
                </div>
                <div className="text">
                    <div className="subtext">
                        <p>La fisiopatología de
                        <br />la osteoartritis <br /> comprende un <br /> proceso <br /> inflamatorio y un <br /> proceso <br /> degenerativo
                    </p>
                    </div>
                </div>
                <div className="text1">
                    <div className="subtext1">
                        <p>El factor nuclear kB
                        <br />promueve la síntesis <br /> del IL-1, TNF y <br /> aumenta el estrés <br /> oxidativo
                    </p>
                    </div>
                </div>
                <div className="blanco"></div>

                <div className="text2">
                    <div className="subtext2">
                        <p>El TNF favorece la
                        <br />actividad de la COX- <br /> 2 que a su vez <br /> produce <br /> prostaglandinas <br/> (PGE)
                    </p>
                    </div>
                </div>
                <div className="blanco2"></div>

                <div className="text3">
                    <div className="subtext3">
                        <p>el estrés oxidativo
                        <br /> produce Oxido <br /> nítrico (ON) <br />
                    </p>
                    </div>
                </div>

                <div className="text4">
                    <div className="subtext4">
                        <p>el Oxido n+itrico (ON)
                        <br /> y las  <br /> prostaglandinas <br /> (PGE) activan a las <br/> enzimas que <br/> degradan a los <br/> proteoglicanos y al <br/> acido hialurónico <br/> Produciendo <br/> degeneración del <br/>
                        cartílago y <br/> osteoartritis
                    </p>
                    </div>
                </div>

                <div className="text5">
                    <div className="subtext5">
                        <p>La glucosamina
                        <br /> inhibe al factor <br /> nuclear kB y <br/> disminuye el estres <br/> oxidativo lo que <br/> evita la activiación <br/> de las enzimas que  <br/> degradan al <br/> cartílago
                    </p>
                    </div>
                </div>

                <div className="text6">
                    <div className="subtext6">
                        <p>Además la
                        <br /> glucosamina es  <br /> componente de los <br/> proteoglicanos y del <br/> ácido hialurónico
                    </p>
                    </div>
                </div>

                <div className="text7">
                    <div className="subtext7">
                        <p>El meloxican inhibe
                        <br /> a la COX-2 <br /> disminuyendo la <br/> producción de <br/> prostaglandinas y <br/> evita la activación <br/> de las enzimas que <br/> degradan al <br/> cartílago
                    </p>
                    </div>
                </div>

                <div className="text8">
                    <div className="subtext8">
                        <p>Además meloxicam
                        <br /> favorece la síntesis <br /> de proteoglicanos y <br/> del acido <br/> hialurónico
                    </p>
                    </div>
                </div>

                <Link to="/Vartalon-2">
                    <div className="link10"></div>
                </Link>
            </Col>
        );
    }
}

export default Vartalon4;