import React from 'react';
import { Col } from 'reactstrap';
import $ from 'jquery';
import { Link } from "react-router-dom";


class Vartalon3 extends React.Component {
    componentDidMount() {
        $('body').removeAttr('id');
        this.changePage();
        this.mountPage();
    }

    changePage(){
        $(document).ready(function(){
            $(document).on('touchmove', function(){
                window.location = '/vartalon-7';
            })
        })
    }

    mountPage(){
        let bandera = 0;
        setInterval(() => {
            if(bandera == 0){
                $('.pol').css('visibility', 'visible').addClass('animate__backInDown');
                bandera++
            }else {
                if(bandera == 1){
                    $('.giro').css('visibility', 'visible').addClass('animate__zoomInRight');
                }
            }
        }, 2000);
    }

    render(){
        return(
            <Col className="vartalon6" lg="12" md="12" xs="12" sm="12">
                <img src="images/grafica.png" className="giro animate__animated" alt="vartalon"/>
                <img src="images/imagen-piki.png" className="pol animate__animated" alt="vartalon"/>

                <Link to="/Vartalon-2">
                    <div className="link5"></div>
                </Link>
            </Col>
            
        );  
    }
}

export default Vartalon3; 