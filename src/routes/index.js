import React from 'react';
import {BrowserRouter as Router, Switch, Route,} from 'react-router-dom';
import Vartalon from '../views/vartalon';
import Vartalon2 from '../views/vartalon-2';
import Vartalon3 from '../views/vartalon-3';
import Vartalon4 from '../views/vartalon-4';
import Vartalon5 from '../views/vartalon-5';
import Vartalon6 from '../views/vartalon-6';
import Vartalon7 from '../views/vartalon-7';
import Vartalon8 from '../views/vartalon-8';

export default function Routes() {
    return (
        <React.Fragment>
            <Router>
                <Switch>
                    <Route path="/" exact  component={ Vartalon }/>
                    <Route path="/vartalon-2" exact  component={ Vartalon2 }/>
                    <Route path="/vartalon-3" exact  component={ Vartalon3 }/>
                    <Route path="/vartalon-4" exact  component={ Vartalon4 }/>
                    <Route path="/vartalon-5" exact  component={ Vartalon5 }/>
                    <Route path="/vartalon-6" exact  component={ Vartalon6 }/>
                    <Route path="/vartalon-7" exact  component={ Vartalon7 }/>
                    <Route path="/vartalon-8" exact  component={ Vartalon8 }/>
                </Switch>
            </Router>
        </React.Fragment>
    )
}
